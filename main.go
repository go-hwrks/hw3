package main

import (
	"fmt"
	"math"
	"os"
	"os/exec"
)

const (
	addition   = "Сложить"
	subtract   = "Вычесть"
	multiply   = "Умножить"
	divide     = "Разделить"
	percent    = "Взять процент"
	squareRoot = "Корень по основанию"
	power      = "Возвести в степень"
	simple     = "Получить простые числа"
)

func main() {
	var a, b, res float64

	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()

	act := menu()

	switch act {
	case 1:
		res = add(&a, &b)
	case 2:
		subt(&a, &b, &res)
	case 3:
		mult(&a, &b, &res)
	case 4:
		div(&a, &b, &res)
	case 5:
		perc(&a, &b, &res)
	case 6:
		sqroot(&a, &b, &res)
	case 7:
		res = pow(a, b)
	case 8:
		simpleNum()
		os.Exit(0)
	default:
		fmt.Println("Операция выбрана неверно")
		os.Exit(0)
	}

	fmt.Printf("Результат выполнения операции: %.2f\n", res)
}

func menu() int {
	actions := [...]string{
		addition,
		subtract,
		multiply,
		divide,
		percent,
		squareRoot,
		power,
		simple,
	}

	for i, j := range actions {
		fmt.Printf("%d - %s\n", i+1, j)
	}
	fmt.Printf("Введите арифметическую операцию: ")
	var act int
	fmt.Scanln(&act)
	return act
}

// В этой функции пробую поработать с указателями, но возвращать значение.
func add(a *float64, b *float64) float64 {
	fmt.Print("введите первое слагаемое ")
	fmt.Scanln(a)
	fmt.Print("введите второе слагаемое ")
	fmt.Scanln(b)
	res := *a + *b
	return res
}

// В этой функции пробую поработать с указателями без прямого возврата.
func subt(a *float64, b *float64, res *float64) {
	fmt.Print("введите первое число ")
	fmt.Scanln(a)
	fmt.Print("введите второе число ")
	fmt.Scanln(b)
	*res = *a - *b
}

func mult(a *float64, b *float64, res *float64) {
	fmt.Print("введите первый множитель ")
	fmt.Scanln(a)
	fmt.Print("введите второй множитель ")
	fmt.Scanln(b)
	*res = *a * *b
}

func div(a *float64, b *float64, res *float64) {
	fmt.Print("введите делимое ")
	fmt.Scanln(a)
	fmt.Print("введите делитель ")
	fmt.Scanln(b)
	*res = *a / *b
}

func perc(a *float64, b *float64, res *float64) {
	fmt.Print("введите число ")
	fmt.Scanln(a)
	fmt.Print("введите процент который вы хотите получить ")
	fmt.Scanln(b)
	*res = *a / 100 * *b
}

func sqroot(a *float64, b *float64, res *float64) {
	fmt.Print("введите число ")
	fmt.Scanln(a)
	fmt.Print("введите степень корня ")
	fmt.Scanln(b)
	*res = math.Pow(*a, 1.0 / *b)
}

func pow(a float64, b float64) float64 {
	fmt.Print("введите число ")
	fmt.Scanln(&a)
	fmt.Print("введите степень ")
	fmt.Scanln(&b)
	res := math.Pow(a, b)
	return res
}

func simpleNum() {
	fmt.Print("введите число чтобы узнать сколько простых чисел от 1 до него ")
	var a int
	k := 0
	var lst = []int{}
	fmt.Scanln(&a)
	for i := 1; i <= a; i++ {
		for j := 1; j <= i; j++ {
			if i%j == 0 && i > 2 {
				k += 1
			}
		}
		if k <= 2 {
			lst = append(lst, i)

		} else {
			k = 0
		}
	}
	fmt.Println("Простые числа: ", lst)
}
